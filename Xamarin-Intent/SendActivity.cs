﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using System;
using Android.Runtime;

namespace Xamarin_Intent
{
    [Activity(Label = "Xamarin-Intent", MainLauncher = true)]
    public class SendActivity : Activity
    {
        string sendAction;

        const string SEND_IN          = "com.asmasyakirah.xamarin_intent.sendIn";
        const string SEND_IN_RESPOND  = "com.asmasyakirah.xamarin_intent.sendInRespond";
        const string SEND_OUT         = "com.asmasyakirah.xamarin_intent.sendOut";
        const string SEND_OUT_RESPOND = "com.asmasyakirah.xamarin_intent.sendOutRespond";

        const int SEND_IN_REQUEST  = 0;
        const int SEND_OUT_REQUEST = 1;

        EditText sendInput;
        Button sendInButton, sendInRespondButton, sendOutButton, sendOutRespondButton;
        private EventHandler<DialogClickEventArgs> onOK;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetupUI();
        }

        private void SetupUI()
        {
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Send);

            sendInput = FindViewById<EditText>(Resource.Id.sendInput);
            sendInButton = FindViewById<Button>(Resource.Id.sendInButton);
            sendInRespondButton = FindViewById<Button>(Resource.Id.sendInRespondButton);
            sendOutButton = FindViewById<Button>(Resource.Id.sendOutButton);
            sendOutRespondButton = FindViewById<Button>(Resource.Id.sendOutRespondButton);

            // Button click respond.
            sendInButton.Click += (s, e) =>
            {
                sendAction = SEND_IN;
                SetIntent();
            };
            sendInRespondButton.Click += (s, e) =>
            {
                sendAction = SEND_IN_RESPOND;
                SetIntent();
            };
            sendOutButton.Click += (s, e) =>
            {
                sendAction = SEND_OUT;
                SetIntent();
            };
            sendOutRespondButton.Click += (s, e) =>
            {
                sendAction = SEND_OUT_RESPOND;
                SetIntent();
            };
        }

        private void SetIntent()
        {
            if (sendInput.Text != "")
            {
                Intent intent;

                switch (sendAction)
                {
                    case SEND_IN:
                        intent = new Intent(this, typeof(ReceiveActivity));
                        intent.SetAction(sendAction);
                        intent.PutExtra("DATA", sendInput.Text.ToString());
                        StartActivity(intent);
                        break;

                    case SEND_IN_RESPOND:
                        intent = new Intent(this, typeof(ReceiveActivity));
                        intent.SetAction(sendAction);
                        intent.PutExtra("DATA", sendInput.Text.ToString());
                        StartActivityForResult(intent, SEND_IN_REQUEST);
                        break;

                    case SEND_OUT:
                        intent = new Intent();
                        intent.SetAction(sendAction);
                        intent.PutExtra("DATA", sendInput.Text.ToString());
                        try
                        {
                            StartActivity(intent);
                        }
                        catch (Exception ex)
                        {
                            ShowMessage(ex.Message);
                        }
                        break;

                    case SEND_OUT_RESPOND:
                        intent = new Intent();
                        intent.SetAction(sendAction);
                        intent.PutExtra("DATA", sendInput.Text.ToString());
                        try
                        {
                            StartActivityForResult(intent, SEND_OUT_REQUEST);
                        }
                        catch (Exception ex)
                        {
                            ShowMessage(ex.Message);
                        }
                        break;
                }
            }
            else
            {
                ShowMessage("Please key in data");
            }
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == SEND_IN_REQUEST)
            {
                if (resultCode == Result.Ok)
                {
                    string respondData = data.GetStringExtra("DATA");
                    ShowMessage("OK! Respond is \"" + respondData + "\"");
                }
                if (resultCode == Result.Canceled)
                {
                    ShowMessage("Cancelled");
                }
                else if ((int)resultCode == 1)
                {
                    string err = data.GetStringExtra("ERRORMSG");
                    ShowMessage("Error: " + err);
                }
            }

            if (requestCode == SEND_OUT_REQUEST)
            {
                if (resultCode == Result.Ok)
                {
                    string respondData = data.GetStringExtra("DATA");
                    ShowMessage("OK! Respond is \"" + respondData + "\"");
                }
                if (resultCode == Result.Canceled)
                {
                    ShowMessage("Cancelled");
                }
                else if ((int)resultCode == 1)
                {
                    string err = data.GetStringExtra("ERRORMSG");
                    ShowMessage("Error: " + err);
                }
            }
        }

        private void ShowMessage(string message)
        {
            //Toast.MakeText(this, message, ToastLength.Short).Show();

            onOK = new EventHandler<DialogClickEventArgs>((o, a) => { });
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.SetMessage(message);
            dialog.SetPositiveButton("OK", onOK);
            dialog.Show();
        }
    }
}

