﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using Android.Views;
using Android.Content;

namespace Xamarin_Intent
{
    [Activity(Label = "Xamarin-Intent")]
    public class ReceiveActivity : Activity
    {
        string receivedAction;
        string receivedFrom;
        string receivedData;
        
        const string RECEIVE_IN          = "com.asmasyakirah.xamarin_intent.sendIn";
        const string RECEIVE_IN_RESPOND  = "com.asmasyakirah.xamarin_intent.sendInRespond";
        const string RECEIVE_OUT         = "com.asmasyakirah.android_intent.sendOut";
        const string RECEIVE_OUT_RESPOND = "com.asmasyakirah.android_intent.sendOutRespond";

        TextView receivedFromTextView;
        TextView receivedTextView;
        LinearLayout respondLayout;
        EditText respondInput;
        Button okButton;
        Button cancelButton;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetupUI();
            GetIntent();
            SetIntent();
        }

        private void SetupUI()
        {
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Receive);

            receivedFromTextView = FindViewById<TextView>(Resource.Id.receivedFromTextView);
            receivedTextView = FindViewById<TextView>(Resource.Id.receivedTextView);
            respondLayout = FindViewById<LinearLayout>(Resource.Id.respondLayout);
            respondInput = FindViewById<EditText>(Resource.Id.respondInput);
            okButton = FindViewById<Button>(Resource.Id.okButton);
            cancelButton = FindViewById<Button>(Resource.Id.cancelButton);
        }

        private void GetIntent()
        {
            receivedAction = Intent.Action;
            receivedFrom = "Received data";
            receivedData = Intent.GetStringExtra("DATA");

            switch (receivedAction)
            {
                case RECEIVE_IN:
                    break;

                case RECEIVE_IN_RESPOND:
                    respondLayout.Visibility = ViewStates.Visible;
                    break;

                case RECEIVE_OUT:
                    receivedFrom = receivedFrom + " from " + receivedAction;
                    break;

                case RECEIVE_OUT_RESPOND:
                    receivedFrom = receivedFrom + " from " + receivedAction;
                    break;
            }

            SetOutput();
        }

        private void SetOutput()
        {
            receivedFromTextView.Text = receivedFrom;
            receivedTextView.Text = receivedData;
        }

        private void SetIntent()
        {
            Intent respondIntent = new Intent();

            // Button click respond.
            okButton.Click += (s, e) =>
            {
                respondIntent.PutExtra("DATA", respondInput.Text.ToString());
                SetResult(Result.Ok, respondIntent);
                Finish();
            };
            cancelButton.Click += (s, e) =>
            {
                SetResult(Result.Canceled, respondIntent);
                Finish();
            };
        }
    }
}