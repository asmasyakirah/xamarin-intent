package md5a206425053037e05b5e8775303bbdb42;


public class ReceiveActivity
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("Xamarin_Intent.ReceiveActivity, Xamarin-Intent, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", ReceiveActivity.class, __md_methods);
	}


	public ReceiveActivity () throws java.lang.Throwable
	{
		super ();
		if (getClass () == ReceiveActivity.class)
			mono.android.TypeManager.Activate ("Xamarin_Intent.ReceiveActivity, Xamarin-Intent, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
